import ctypes
import numpy
import glob

libfile = glob.glob('build/*/mysum*.so')[0]

mylib = ctypes.CDLL(libfile)

mylib.mysum.restype = ctypes.c_longlong
mylib.mysum.argtypes = [ctypes.c_int,
                        numpy.ctypeslib.ndpointer(dtype=numpy.int32)]

array = numpy.arange(0, 100000000, 1, numpy.int32)

array_sum = mylib.mysum(len(array), array)

print('sum of array: {}'.format(array_sum))

