import ctypes
import numpy
import glob

libfile = glob.glob('build/*/add*.so')[0]

addlib = ctypes.CDLL(libfile)

addlib.add.restype = ctypes.c_int

addlib.add.argtypes = [ctypes.c_int, ctypes.c_int]

a = 5
b = 4

result = addlib.add(a,b)
print('sum of 2 numbers is:{} '.format(result))
