import numpy as np
import ctypes
from ctypes import *


def get_reduce_by_key_with_zeroes():
    dll = ctypes.CDLL('./reduce_by_key_with_zeroes.so')
    func = dll.reduce_by_key_with_zeroes
    return func


if __name__ == '__main__':
    my_func = get_reduce_by_key_with_zeroes()
    my_func()
