import numpy as np
import ctypes
from ctypes import *


def get_reduce_by_key_with_zeroes():
    dll = ctypes.CDLL('./reduce_by_key_with_zeroes.so')
    func = dll.reduce_by_key_with_zeroes
    func.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int]
    return func


if __name__ == '__main__':
    num = 10
    num2 = 3
    arr1 = [1,2,3,5,7]
    np1 = len(arr1)
    print('Run myfunc')
    my_func = get_reduce_by_key_with_zeroes()
    print('Initialization')
    my_func(np1,num, num2)
    print('execution')
