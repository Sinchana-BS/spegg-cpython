#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>


extern "C" {

void reduce_by_key_with_zeroes(int np1, int a, int b)
        {
        std::cout << "Hi there" << std::endl;

        //int arr[10];
        int arr[np1];
        int num = 10;
        //int num = a;
        int arr1[20];
        int num1 = 20;
        int arr2[3];
        //int num2 = 3;
        int num2 = b;


        int number_of_values = 10;
        int number_of_possible_keys = 3;
        thrust::host_vector<int> H(4);

        H[0] = 14;
        H[1] = 20;
        H[2] = 38;
        H[3] = 46;



       std::cout << "H has size " << H.size() << std::endl;

       for(int i = 0; i < num; i++)
        {
        arr[i] = i*2;
                   }

      for(int i = 0; i < num1; i++)
        {
        arr1[i] = i*2;
        }
      for(int i = 0; i < num2; i++)
        {
        arr2[i] = i*2;
        }
        thrust::device_vector<int> D(arr, arr+num);

      D[0] = 1; // actual keys
      D[1] = 1;
      D[2] = 0;

      thrust::device_vector<int> E(arr1, arr1+num1);

     E[0] = 12; // values
     E[1] = 13;
     E[2] = 15;

     thrust::device_vector<int> F(arr2, arr2+num2);

     F[0] = 90;
     F[1] = 80;


    for(int i = 0; i < D.size(); i++)
        std::cout << "D[" << i << "] = " << D[i] << std::endl;

        thrust::device_vector<int> key_copy(number_of_possible_keys);
        key_copy.erase(thrust::unique_copy(D.begin(), D.begin() + number_of_values, key_copy.begin()), key_copy.end());

        int number_of_keys_available = thrust::distance(key_copy.begin(), key_copy.end());

        thrust::device_vector<int> key_outputs(number_of_keys_available);
        thrust::device_vector<int> reduction_results(number_of_keys_available);

        thrust::reduce_by_key(D.begin(), D.begin() + number_of_values, E.begin(), key_outputs.begin(), reduction_results.begin());

        F.resize(number_of_possible_keys);

        thrust::fill(F.begin(), F.end(), 0);

        thrust::scatter(reduction_results.begin(), reduction_results.end(), key_outputs.begin(), F.begin());


std::cout << "After reduce by key:" << std::endl;
    for (int i=0; i < F.size(); i++)
            std::cout << "Key number: " << i << " has sum of " <<  F[i] << std::endl;

        }
}

