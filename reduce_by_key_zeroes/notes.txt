This folder is an complete example of calling cuda file from python! 

we are passing arrays and ints from python to cuda file where its converted to vectors for operation.

Instructions to run the programs:

nvcc -Xcompiler -fPIC -shared -o reduce_by_key_with_zeroes.so reduce_by_key_zeroes.cu

python reduce_by_key_zeroes.py

Note: reduce_by_key_zeroes.h is a header file which is included in cuda file